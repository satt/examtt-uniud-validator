import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="examtt-uniud-validator", 
    version="0.9",
    author="Luca Di Gaspero",
    author_email="luca.digaspero@uniud.it",
    description="An instance and solution validator for the Udine's examination timetabling problem",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/satt/examtt-uniud-validator",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering"
    ],
    python_requires='>=3.5',
    entry_points = {
        'console_scripts': ['examtt-uniud-validator=examtt_uniud_validator.solution_validator:main'],
    }
)