import json, re
from itertools import combinations
import math
from collections import defaultdict

class Course(object):
    def __init__(self, teachers, **kwargs):
        self.name = kwargs.get('Course')
        self.parts = kwargs.get('ExamType').split('And')
        self.number_of_exams = kwargs.get('NumberOfExams')
        self.distances_between_exams = (kwargs.get('MinimumDistanceBetweenExams', 0), kwargs.get('MaximumDistanceBetweenExams', math.inf))
        if kwargs.get('WrittenOralSpecs') is not None:
            self.part_distances = [(kwargs.get('WrittenOralSpecs').get('MinDistance', 0), kwargs.get('WrittenOralSpecs').get('MaxDistance', math.inf))]
            self.same_day = kwargs.get('WrittenOralSpecs').get('SameDay', False)
        else:
            self.part_distances = [(0, math.inf) for _ in range(len(self.parts) - 1)] # in form of (min, max), given for consecutive parts (therefore len(self.parts) - 1)
            self.same_day = False            
        self.rooms = []
        if kwargs.get('RoomsRequested') is not None and kwargs.get('RoomsRequested').get('Number') > 0:
            # assume that the 'Written' is always the first part and the room request refers mainly to that part
            self.rooms.append((kwargs.get('RoomsRequested').get('Type'), kwargs.get('RoomsRequested').get('Number')))
            if len(self.parts) == 2:
                if kwargs.get('WrittenOralSpecs') is not None and kwargs.get('WrittenOralSpecs').get('RoomForOral', False):
                    self.rooms.append(('Small', 1)) # make a duplicate of the room request also for the oral
                else:
                    self.rooms.append(None) # no room required for that part (the oral)
        else:
            self.rooms = [None] * len(self.parts)
            if kwargs.get('WrittenOralSpecs') is not None and kwargs.get('WrittenOralSpecs').get('RoomForOral', False):
                self.rooms[self.parts.index('Oral')] = ('Small', 1)
        _teacher = kwargs.get('Teacher')
        if _teacher not in teachers:
            raise ValueError("Teacher of course {} ({}) is not present in the list of teachers".format(self.name, _teacher))
        self.teacher = _teacher
        self.period_constraints = [defaultdict(lambda: []) for __ in range(len(self.parts))]
        self.room_constraints = defaultdict(lambda: [None for __ in range(len(self.parts))])

        
    def __repr__(self):
        return "Name: {}, {}, Parts: {}, Distances: {}/{}, Rooms: {}".format(self.name, \
            self.number_of_exams, self.parts, self.distances_between_exams, \
            self.part_distances, self.rooms)

class Room(object): 
    def __init__(self, **kwargs):
        self.name = kwargs.get('Room')
        self.type = kwargs.get('Type')
        self.members = map(lambda r: r, kwargs.get('Members', [])) 
        self.constraints = {}

    def dispatch_composite(self, rooms):
        if self.type != 'Composite':
            return
        _members = set(map(lambda r: rooms.get(r), self.members))
        if None in _members:
            raise ValueError('There are one or more wrong room members in composite room {}'.format(_members))
        self.members = _members
         
    def __repr__(self):
        res = "Room: {}, Type: {}".format(self.name, self.type)
        if self.type == 'Composite':
            return "{}, Members: {}".format(res, self.members)
        else:
            return res
            

class Instance(object):
    '''Class representing a problem instance of the Graduation Timetabling Problem'''    

    formats = ['.json']
    '''List of available formats (str)'''

    default_format = '.json'
    '''Default format (str)'''
     
    @staticmethod
    def un_camel(input):
        output = [input[0].lower()]
        for c in input[1:]:
            if c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
                output.append('_')
                output.append(c.lower())
            else:
                output.append(c)
        return str.join('', output)
    

    def __init__(self, inst_content, inst_format=None):
        '''Loads an instance object from the instance content as string'''

        if inst_format not in self.formats:
            raise ValueError('Unsupported instance format: ' + str(inst_format) + '. Try one of {' + ','.join(map(str,self.formats)) + '} instead.')

        data = json.loads(inst_content)    
        self.periods = data.get('Periods')
        self.slots_per_day = data.get('SlotsPerDay')
        self.teachers = set(data.get('Teachers'))       
        self.courses = {c.name: c for c in map(lambda d: Course(self.teachers, **d), data.get('Courses', []))}
        self.primary_primary_distance = data.get('PrimaryPrimaryDistance')
        self.rooms = {r.name: r for r in map(lambda d: Room(**d), data.get('Rooms', []))}
        for r in self.rooms.values():
            r.dispatch_composite(self.rooms)
        self.period_constraints = {}

        # Dispatch all constraints
        for c in data.get('Constraints', []):
            try:
                _period = c.get('Period')
                if 'Period' in c.get('Type') and (_period < 0 or _period >= self.periods):
                    raise ValueError("Wrong period {} in constraint {}".format(_period, json.dumps(c)))
                _level = c.get('Level')
                if _level not in ("Forbidden", "Undesired", "Preferred"):
                    raise ValueError("Wrong constraint level {} in constraint {}".format(_level, json.dumps(c)))
                if c.get('Type') == 'EventPeriodConstraint':
                    course = self.courses[c.get('Course')]
                    _parts = [c.get('Part')]
                    # TODO: currently if the part is not present it means all parts
                    if _parts[0] is None:
                        _parts = course.parts
                    elif _parts[0] not in course.parts:
                        raise ValueError("Wrong constraint, part {} is not part of the course {}, {}".format(_parts[0], course.name, json.dumps(c)))
                    for part in _parts:
                        course.period_constraints[course.parts.index(part)][_period].append((c.get('Exam', 1), _level))
                    # TODO: check that there are no multiple constraints for the same course/exam/part/period combination
                elif c.get('Type') == 'PeriodConstraint':
                    if _period in self.period_constraints:
                        raise ValueError('Multiple PeriodConstraint definitions for the same period')
                    self.period_constraints[_period] = _level
                elif c.get('Type') == 'RoomPeriodConstraint':
                    room = self.rooms[c['Room']]
                    if _period in room.constraints:
                        raise ValueError('Multiple RoomPeriodConstraint definitions for the same period')
                    room.constraints[_period] = _level
                elif c.get('Type') == 'EventRoomConstraint':
                    course = self.courses[c.get('Course')]
                    _parts = [c.get('Part')]
                    # TODO: currently if the part is not present it means all parts
                    if _parts[0] is None:
                        _parts = course.parts
                    elif _parts[0] not in course.parts:
                        raise ValueError("Wrong constraint, part {} is not part of the course {}, {}".format(_parts[0], course.name, json.dumps(c)))
                    for part in _parts:
                        course.room_constraints[c['Room']][course.parts.index(part)] = _level
                else:
                    raise ValueError("Wrong constraint type {}".format(c.get('Type')))  
            except Exception as e:
                print("Constraint {} is wrong {}".format(c, e))
                raise e
        # Dispatch curricula
        self.curricula = {}
        for q in data.get('Curricula', []):
            self.curricula[q.get('Curriculum')] = {}
            _members = set(map(lambda c: self.courses.get(c), q.get('PrimaryCourses', [])))
            if None in _members:
                raise ValueError('There are one or more wrong primary courses in curriculum {}: {}'.format(q.get('Curriculum'), c.get('PrimaryCourses')))
            self.curricula[q.get('Curriculum')]['primary'] = _members
            _members = set(map(lambda c: self.courses.get(c), q.get('SecondaryCourses', [])))
            if None in _members:
                raise ValueError('There are one or more wrong secondary courses in curriculum {}: {}'.format(q.get('Curriculum'), c.get('SecondaryCourses')))
            self.curricula[q.get('Curriculum')]['secondary'] = _members
        
        # post-processing
        # check room availability
        for course in self.courses.values():
            for part in range(len(course.parts)):
                if course.rooms[part] is None:
                    continue
                type, number = course.rooms[part]
                compatible_rooms = set(filter(lambda r: r.type == type, self.rooms.values()))
                if len(compatible_rooms) < number:
                    raise ValueError('Course {} requires {} rooms of type {} for {} but only {} are available'.format(course.name, number, type, course.parts[part], len(compatible_rooms)))                                    
                
        # explicitly compute the set of available periods for each course (not forbidden) 
        for course in self.courses.values():
            # domain consistency: remove forbidden values
        #    start_part = course.parts[0]
            course.available_periods = [[set(range(self.periods)) for __ in range(len(course.parts))] for ___ in range(course.number_of_exams)]
            for part in range(len(course.parts)):
                for period, constraints in course.period_constraints[part].items():
                    for (exam, level) in constraints:
                        if level != 'Forbidden':
                            continue
                        course.available_periods[exam][part].discard(period)
            # second part of domain consistency: remove periods if not enough rooms are available at that time
            for part in range(len(course.parts)):
                if course.rooms[part] is None:
                    continue
                type, number = course.rooms[part]
                compatible_rooms = set(filter(lambda r: r.type == type, self.rooms.values()))
                for exam in range(course.number_of_exams):
                    periods_to_remove = []
                    for p in course.available_periods[exam][part]:
                        available_rooms_at_p = sum(1 if r.constraints.get(p) != 'Forbidden' else 0 for r in compatible_rooms)
                        if available_rooms_at_p < number:
                            periods_to_remove.append(p)
                    for p in periods_to_remove:
                            course.available_periods[exam][part].discard(p)
            # third part of domain consistency: if it's a sameday exam, then some periods can be removed
            if course.same_day:
                parts = len(course.parts)
                available_slots = [set(range(self.slots_per_day)) for __ in range(parts)]
                for part in range(parts):
                    for slot in range(self.slots_per_day):
                        if slot < part:
                            available_slots[part].discard(slot)
                        remaining_parts = parts - 1 - part
                        if slot + remaining_parts >= self.slots_per_day:
                            available_slots[part].discard(slot)
                    for d in range(self.periods // self.slots_per_day):
                        for s in range(self.slots_per_day):
                            period = d * self.slots_per_day + s
                            if s not in available_slots[part]:
                                for exam in range(course.number_of_exams):
                                    course.available_periods[exam][part].discard(period)
                
            # filter out constraints (min distance, and also room unavailabilities)            
            def filter_constraint(D0, D1, t):
                a, b = t
                # consider the constraint $a \leq x_1 - x_0 \leq b$
                # these are the propagation rules:
                d0 = len(D0)
                d1 = len(D1)
                # propagation rules for $x_0 + a \leq x_1$
                # 1.1 $\forall x \in D_0 \quad x + a \leq \max D_1$:
                D0 = set(filter(lambda x: x <= max(D1) - a, D0))
                # 1.2. $\forall x \in D_1 \quad \min D_0 + a \leq x$ 
                D1 = set(filter(lambda x: min(D0) + a <= x, D1))
                # propagation rules for $x_1 \leq x_0 + b$
                # 2.1. $\forall x \in D_1 \quad x \leq \max D_0 + b$
                D1 = set(filter(lambda x: x <= max(D0) + b, D1))
                # 2.2. $\forall x \in D_0 \quad \min D_1 \leq x + b$
                D0 = set(filter(lambda x: min(D1) - b <= x, D0))
                return len(D0) < d0 or len(D1) < d1, D0, D1
                
              
            changed = True
            # constraint propagation up to fixpoint
            try:                
                while changed:
                    changed = False
                    # first ensure that the difference between the first parts is kept
                    part = 0
                    for exam_a, exam_b in combinations(range(course.number_of_exams), 2):
                        c, course.available_periods[exam_a][part], course.available_periods[exam_b][part] = filter_constraint(course.available_periods[exam_a][part], course.available_periods[exam_b][part], course.distances_between_exams)
                        changed = changed or c
                    # secondly ensure that the inner distance between parts is kept
                    for exam in range(course.number_of_exams):                    
                        for part_a in range(len(course.parts) - 1):
                            part_b = part_a + 1
                            c, course.available_periods[exam][part_a], course.available_periods[exam][part_b] = filter_constraint(course.available_periods[exam][part_a], course.available_periods[exam][part_b], course.part_distances[part_a])
                            changed = changed or c
                                                  
            except ValueError: # a domain has become empty
                pass
                                            

    def __str__(self):
        return '{}'.format(self.periods)
