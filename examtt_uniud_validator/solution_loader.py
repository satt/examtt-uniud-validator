import json, re

def un_camel(input):
    output = [input[0].lower()]
    for c in input[1:]:
        if c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
            output.append('_')
            output.append(c.lower())
        else:
            output.append(c)
    return str.join('', output)

class EventAssignment(object):
    def __init__(self, **kwargs):
        for k in kwargs:
            setattr(self, un_camel(k), kwargs[k])
        if not hasattr(self, 'room'):
            self.room = None
    
    def __repr__(self):
        return "<{} {} {} {}>".format(self.exam, self.part, self.period, self.room)    

class Solution(object):
    '''Class representing a problem solution'''

    formats = ['.json']
    '''List of available formats (str)'''

    default_format = formats[0]
    '''Default format (str)'''
    
    def __init__(self, sol_content, sol_format):
        '''Loads a solution object from the solution content as a set of assignments'''

        if sol_format not in self.formats:
            raise ValueError('Unsupported solution format {}. Try one of {} instead'.format(sol_format, '{' + ','.join(map(str,self.formats)) + '}'))

        try:
            data = json.loads(sol_content)
            self.assignments = { a.get('Course'): sorted([EventAssignment(**e) for e in a.get('Events')], key=lambda e: e.exam) for a in data.get('Assignments', []) }
            
        except Exception as e:
            raise ValueError(str(e))