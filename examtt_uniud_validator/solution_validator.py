#!/usr/bin/env python3
import argparse
import json
import math
from itertools import combinations, product
from collections import defaultdict
import logging

try:
    import coloredlogs
    coloredlogs.install()
except ImportError:
    pass

from examtt_uniud_validator.instance_loader import Instance
from examtt_uniud_validator.solution_loader import Solution

log = logging.getLogger('costs')
log.setLevel(logging.INFO)

class SolutionValidator(object):
    '''Solution validation logic'''    

    @classmethod
    def validate(cls, inst_content, inst_format, sol_content, sol_format):
        '''Takes a solution and an instance input as string, evaluates its validity, computes features'''

        # correctly handle None format
        if not inst_format:
            inst_format = Instance.default_format

        # correctly handle None format
        if not sol_format:
            sol_format = Solution.default_format      

        # initialize costs
        cost_components = {}
        cost = None

        instance = Instance(inst_content, inst_format)
        solution = Solution(sol_content, sol_format)

        # basic consistency checkings
        for c, assignment in solution.assignments.items():
            assert c in instance.courses, "Course {} do not belong to the set of courses".format(c)
            course = instance.courses[c]
            assert len(assignment) == len(course.parts) * course.number_of_exams, "Event assignment of course {} are different than those expected {}/{}".format(course.name, len(assignment), len(course.parts) * course.number_of_exams)
            for event in assignment:
                if event.room is not None:
                    assert event.room in instance.rooms, "Room {} do not belong to the set of rooms".format(event.room)
                    room = instance.rooms[event.room]
                else:
                    room = None
                assert event.period >= 0 and event.period < instance.periods
                assert event.exam >= 0 and event.exam < course.number_of_exams                
        # Check hard constraints
        cost_components['hard_components'] = { 
            'both_curriculum_primary_at_same_period': 0,
            'same_teacher_at_same_period': 0,
            'course_forbidden_periods': 0,
            'period_forbidden_rooms': 0,
            'overall_forbidden_periods': 0,
            'course_forbidden_rooms': 0,
            'overall_forbidden_rooms': 0,
            'multiple_room_occupation': 0,
            'unsuitable_rooms': 0,
            'fixed_periods': 0
        }
        cost_components['soft_components'] = { 
            'course_undesired_periods': 0,
            'overall_undesired_periods': 0,
            'course_undesired_rooms': 0,
            'period_undesired_rooms': 0,
            'overall_undesired_rooms': 0,            
            'both_curriculum_primary_and_secondary_at_same_period': 0,
            'both_curriculum_secondary_at_same_period': 0,
            'min_distance_between_exams': 0,
            'max_distance_between_exams': 0,            
            'min_distance_between_parts': 0,
            'max_distance_between_parts': 0,
            'min_distance_between_same_curriculum_primary': 0,
            'min_distance_between_same_curriculum_primary_secondary': 0,
            'not_same_day': 0
        }
        # Curriculum related violations and costs: these are a bit complex because the worst case has to be taken
        curriculum_clashes = {}
        curriculum_distances = {}
        PRIMARY, MIXED, SECONDARY = 2, 1, 0
        MIN_DISTANCE_PRIMARY = instance.primary_primary_distance
        MIN_DISTANCE_PRIMARY_SECONDARY = 1 * instance.slots_per_day
        for curriculum_name, curriculum in instance.curricula.items():
            for course_a, course_b in combinations(curriculum['primary'], 2):
                common_periods = set([a.period for a in solution.assignments[course_a.name]]) & set([a.period for a in solution.assignments[course_b.name]])
                if course_a.name > course_b.name:
                    course_a, course_b = course_b, course_a
                if common_periods:    
                    curriculum_clashes[(course_a.name, course_b.name)] = (PRIMARY, len(common_periods))  
                    log.error('[curricula] curriculum {} clashing primary course {} and {} at {}'.format(curriculum_name, course_a.name, course_b.name, common_periods))              
                first_part_a, first_part_b = course_a.parts[0], course_b.parts[0]
                d = 0
                for p_a, p_b in product([a.period for a in solution.assignments[course_a.name] if a.part == first_part_a], [a.period for a in solution.assignments[course_b.name] if a.part == first_part_b]):
                    if abs(p_a - p_b) < MIN_DISTANCE_PRIMARY:
                        log.info('[curricula] curriculum {} distance between course {}@{} and {}@{} as primary courses under minimum ({})'.format(curriculum_name, course_a.name, p_a, course_b.name, p_b, MIN_DISTANCE_PRIMARY - abs(p_a - p_b)))
                        d += MIN_DISTANCE_PRIMARY - abs(p_a - p_b)
                curriculum_distances[(course_a.name, course_b.name)] = (PRIMARY, d)  
            for course_a, course_b in product(curriculum['primary'], curriculum['secondary']):
                common_periods = set([a.period for a in solution.assignments[course_a.name]]) & set([a.period for a in solution.assignments[course_b.name]])
                if course_a.name > course_b.name:
                    course_a, course_b = course_b, course_a
                if common_periods and (curriculum_clashes.get((course_a.name, course_b.name)) is None or curriculum_clashes[(course_a.name, course_b.name)][0] < PRIMARY):
                    curriculum_clashes[(course_a.name, course_b.name)] = (MIXED, len(common_periods))
                    log.warning('[curricula] curriculum {} clashing primary/secondary course {} and {} at {}'.format(curriculum_name, course_a.name, course_b.name, common_periods))              
                first_part_a, first_part_b = course_a.parts[0], course_b.parts[0]
                d = 0
                for p_a, p_b in product([a.period for a in solution.assignments[course_a.name] if a.part == first_part_a], [a.period for a in solution.assignments[course_b.name] if a.part == first_part_b]):
                    if abs(p_a - p_b) < MIN_DISTANCE_PRIMARY_SECONDARY:
                        if (course_a.name, course_b.name) not in curriculum_distances:
                            log.info('[curricula] curriculum {} distance between course {}@{} and {}@{} as primary/secondary courses under minimum ({})'.format(curriculum_name, course_a.name, p_a, course_b.name, p_b, MIN_DISTANCE_PRIMARY_SECONDARY - abs(p_a - p_b)))
                        d += MIN_DISTANCE_PRIMARY_SECONDARY - abs(p_a - p_b)
                if d > 0 and (curriculum_distances.get((course_a.name, course_b.name)) is None or curriculum_distances[(course_a.name, course_b.name)][0] < PRIMARY):
                    curriculum_distances[(course_a.name, course_b.name)] = (MIXED, d)
            for course_a, course_b in combinations(curriculum['secondary'], 2):
                common_periods = set([a.period for a in solution.assignments[course_a.name]]) & set([a.period for a in solution.assignments[course_b.name]])
                if course_a.name > course_b.name:
                    course_a, course_b = course_b, course_a
                if common_periods and curriculum_clashes.get((course_a.name, course_b.name)) is None:
                    curriculum_clashes[(course_a.name, course_b.name)] = (SECONDARY, len(common_periods))
                    log.warning('[curricula] curriculum {} clashing secondary course {} and {} at {}'.format(curriculum_name, course_a.name, course_b.name, common_periods))              

        for clashes in curriculum_clashes.values():
            if clashes[0] == PRIMARY:
                cost_components['hard_components']['both_curriculum_primary_at_same_period'] += clashes[1]
            elif clashes[0] == MIXED:
                cost_components['soft_components']['both_curriculum_primary_and_secondary_at_same_period'] += clashes[1]
            else:
                cost_components['soft_components']['both_curriculum_secondary_at_same_period'] += clashes[1]
        for distances in curriculum_distances.values():
            if distances[0] == PRIMARY:
                cost_components['soft_components']['min_distance_between_same_curriculum_primary'] += distances[1]
            elif distances[0] == MIXED:
                cost_components['soft_components']['min_distance_between_same_curriculum_primary_secondary'] += distances[1]

        # Teacher related violations
        for course_a, course_b in combinations(solution.assignments, 2):
            course_a, course_b = instance.courses[course_a], instance.courses[course_b]
            if course_a.teacher != course_b.teacher:
                continue
            common_periods = set([a.period for a in solution.assignments[course_a.name]]) & set([a.period for a in solution.assignments[course_b.name]])
            cost_components['hard_components']['same_teacher_at_same_period'] += len(common_periods)
            if common_periods:
                log.error('[teacher] course {} and {} with same teacher {} at same period(s) {}'.format(course_a.name, course_b.name, course_a.teacher, common_periods))
        used_rooms = defaultdict(lambda: [0] * instance.periods)
        for c, assignment in solution.assignments.items():
            course = instance.courses[c]
            for a in assignment:
                if a.period in course.period_constraints[course.parts.index(a.part)]:
                    forbidden = any(True for c in course.period_constraints[course.parts.index(a.part)][a.period] if c[0] == a.exam and c[1] == 'Forbidden')
                    undesired = any(True for c in course.period_constraints[course.parts.index(a.part)][a.period] if c[0] == a.exam and c[1] == 'Undesired')
                    if forbidden:
                        cost_components['hard_components']['course_forbidden_periods'] += 1
                        log.error('[forbidden period] course {}/{} assigned to course forbidden period {}'.format(course.name, a.part, a.period))
                    if undesired:
                        cost_components['soft_components']['course_undesired_periods'] += 1
                        log.info('[undesired period] course {}/{} assigned to course undesired period {}'.format(course.name, a.part, a.period))
                if a.period in instance.period_constraints:
                    forbidden = any(True for l in [instance.period_constraints[a.period]] if l == 'Forbidden')
                    undesired = any(True for l in [instance.period_constraints[a.period]] if l == 'Undesired')
                    if forbidden:
                        cost_components['hard_components']['overall_forbidden_periods'] += 1
                        log.error('[forbidden period] course {}/{} assigned to overall forbidden period {}'.format(course.name, a.part, a.period))
                    if undesired:
                        cost_components['soft_components']['overall_undesired_periods'] += 1
                        log.info('[undesired period] course {}/{} assigned to overall undesired period {}'.format(course.name, a.part, a.period))
                if a.room in course.room_constraints and course.room_constraints[a.room][course.parts.index(a.part)] is not None:
                    if course.room_constraints[a.room][course.parts.index(a.part)] == 'Forbidden':
                        cost_components['hard_components']['course_forbidden_rooms'] += 1
                        log.error('[forbidden room] course {}/{} assigned to course forbidden room {}'.format(course.name, a.part, a.room))
                    if course.room_constraints[a.room][course.parts.index(a.part)] == 'Undesired':
                        cost_components['soft_components']['course_undesired_rooms'] += 1
                        log.info('[undesired room] course {}/{} assigned to course undesired room {}'.format(course.name, a.part, a.room))     
                if a.room is None:
                    if course.rooms[course.parts.index(a.part)] is not None:
                        cost_components['hard_components']['unsuitable_rooms'] += course.rooms[course.parts.index(a.part)][1]
                        log.error('[unsuitable room] course {}/{} has been assigned to no room, while requesting {}'.format(course.name, a.part, course.rooms[course.parts.index(a.part)]))
                    continue
                if course.rooms[course.parts.index(a.part)] is None:
                    log.error('[unsuitable room] course {}/{} has been assigned a room while requesting None'.format(course.name, a.part))
                    cost_components['hard_components']['unsuitable_rooms'] += 1
                    continue
                required_rooms_type = course.rooms[course.parts.index(a.part)][0]
                required_rooms_number = course.rooms[course.parts.index(a.part)][1]
                suitable_assigned_rooms = 0
                room = instance.rooms[a.room]
                if a.period in room.constraints:
                    if room.constraints[a.period] == 'Forbidden':
                        cost_components['hard_components']['period_forbidden_rooms'] += 1
                        log.info('[forbidden room] course {}/{} assigned to room {} forbidden at period {}'.format(course.name, a.part, a.room, a.period))
                    elif room.constraints[a.period] == 'Undesired':
                        cost_components['soft_components']['period_undesired_rooms'] += 1
                        log.info('[undesired room] course {}/{} assigned to room {} undesired at period {}'.format(course.name, a.part, a.room, a.period))
                if room.type != 'Composite':
                    used_rooms[room.name][a.period] += 1
                    if required_rooms_type == 'Small' or (required_rooms_type == 'Medium' and room.type != 'Small') or (required_rooms_type == 'Large' and room.type == 'Large'):
                        suitable_assigned_rooms += 1
                else:
                    for r in room.members:
                        used_rooms[r][a.period] += 1
                        if required_rooms_type == 'Small' or (required_rooms_type == 'Medium' and r.type != 'Small') or (required_rooms_type == 'Large' and r.type == 'Large'):
                            suitable_assigned_rooms += 1
                if suitable_assigned_rooms < required_rooms_number:
                    cost_components['hard_components']['unsuitable_rooms'] += required_rooms_number - suitable_assigned_rooms
                    log.error('[unsuitable room] course {}/{} has been assigned to {} rooms of required type, while requesting {}'.format(course.name, a.part, suitable_assigned_rooms, course.rooms[course.parts.index(a.part)]))
        for r, r_a in used_rooms.items():
            multiple_occupation = sum(max(0, u - 1) for u in r_a)
            if multiple_occupation:
                cost_components['hard_components']['multiple_room_occupation'] += multiple_occupation
                log.error('[multiple occupation] room {} is assigned multiple times {} at the same period'.format(r, multiple_occupation))
        for c, assignment in solution.assignments.items():
            course = instance.courses[c]
            start_periods = list(a.period for a in assignment if a.part == course.parts[0]) # Relies on assignment ordering for the exam number
            for i in range(len(start_periods) - 1):
                if start_periods[i + 1] < start_periods[i] + course.distances_between_exams[0]:
                    cost_components['soft_components']['min_distance_between_exams'] += start_periods[i] + course.distances_between_exams[0] - start_periods[i + 1]
                    log.info('[distance between exam] course {} has exams at {} and {}, under min distance ({})'.format(course.name, start_periods[i], start_periods[i + 1], start_periods[i] + course.distances_between_exams[0] - start_periods[i + 1]))
                if start_periods[i + 1] > start_periods[i] + course.distances_between_exams[1]:
                    cost_components['soft_components']['max_distance_between_exams'] += start_periods[i + 1] - (start_periods[i] + course.distances_between_exams[1])
                    log.info('[distance between exam] course {} has exams at {} and {}, above max distance ({})'.format(course.name, start_periods[i], start_periods[i + 1], start_periods[i + 1] - (start_periods[i] + course.distances_between_exams[1])))
            for exam in range(course.number_of_exams):
                exam_periods = { a.part: a.period for a in assignment if a.exam == exam }
                for i in range(len(course.parts) - 1):
                    part_a = course.parts[i]
                    part_b = course.parts[i + 1]
                    # FIXME: currently in the data it will be counted twice
                    if course.same_day:
                        day_part_a, day_part_b = exam_periods[part_a] // instance.slots_per_day, exam_periods[part_b] // instance.slots_per_day
                        if day_part_a != day_part_b or exam_periods[part_b] <= exam_periods[part_a]:
                            cost_components['soft_components']['not_same_day'] += abs(exam_periods[part_b] - exam_periods[part_a] + instance.slots_per_day - 1)
                            log.info('[not same day] course {} requires same day parts but given {}@{}/{}@{}'.format(course.name, day_part_a, exam_periods[part_a], day_part_b, exam_periods[part_b]))
                    else:
                        # min/max distance                    
                        if exam_periods[part_b] < exam_periods[part_a] + course.part_distances[i][0]:
                            cost_components['soft_components']['min_distance_between_parts'] += exam_periods[part_a] + course.part_distances[i][0] - exam_periods[part_b]
                            log.info('[distance between parts] course {} has exam parts at {} and {}, under min distance ({})'.format(course.name, exam_periods[part_a], exam_periods[part_b], exam_periods[part_a] + course.part_distances[i][0] - exam_periods[part_b]))
                        if exam_periods[part_b] > exam_periods[part_a] + course.part_distances[i][1]:
                            cost_components['soft_components']['max_distance_between_parts'] += exam_periods[part_b] - (exam_periods[part_a] + course.part_distances[i][1])                
                            log.info('[distance between parts] course {} has exam parts at {} and {}, above max distance ({})'.format(course.name, exam_periods[part_a], exam_periods[part_b], exam_periods[part_b] - (exam_periods[part_a] + course.part_distances[i][1])))

        weights = {
            'course_undesired_periods': 10,
            'overall_undesired_periods': 10,
            'course_undesired_rooms': 5,
            'period_undesired_rooms': 5,
            'overall_undesired_rooms': 5,          
            'both_curriculum_primary_and_secondary_at_same_period': 5,
            'both_curriculum_secondary_at_same_period': 1,
            'min_distance_between_exams': 12,
            'max_distance_between_exams': 12,        
            'min_distance_between_parts': 15,
            'max_distance_between_parts': 15,
            'min_distance_between_same_curriculum_primary': 2,
            'min_distance_between_same_curriculum_primary_secondary': 2,
            'not_same_day': 15
        }
        weights = { **weights,
            'both_curriculum_primary_at_same_period': 1,
            'same_teacher_at_same_period': 1,
            'course_forbidden_periods': 1,
            'overall_forbidden_periods': 1,
            'course_forbidden_rooms': 1,
            'overall_forbidden_rooms': 1,
            'multiple_room_occupation': 1,
            'unsuitable_rooms': 1,
            'fixed_periods': 1,
            'period_forbidden_rooms': 1
        }

        cost_components['conflicts'] = 0
        cost_components['distances'] = 0

        cost_components['hard_violations'] = 0
        for component in cost_components['hard_components']:
            cost_components['hard_violations'] += cost_components['hard_components'][component] * weights[component]

        for component in ('both_curriculum_primary_and_secondary_at_same_period', 'both_curriculum_secondary_at_same_period'):
            cost_components['conflicts'] += cost_components['soft_components'][component] * weights[component]
        for component in ('min_distance_between_exams', 'max_distance_between_exams', 'min_distance_between_parts', 'max_distance_between_parts',
                    'min_distance_between_same_curriculum_primary', 'min_distance_between_same_curriculum_primary_secondary', 'not_same_day'
                ):
            cost_components['distances'] += cost_components['soft_components'][component] * weights[component]

        cost_components['soft_violations'] = 0
        for component in cost_components['soft_components']:
            cost_components['soft_violations'] += cost_components['soft_components'][component] * weights[component]
        
        cost = 1000 * cost_components['hard_violations'] + cost_components['soft_violations']

        return {
            'valid': cost_components['hard_violations'] == 0,
            'format': sol_format,
            'cost_components': cost_components,
            'cost': cost
        }

def main():
    '''Entry point for when the solution validator is called from the command line, returns valid JSON'''

    # setup CLI
    parser = argparse.ArgumentParser(description='Examination Timetabling University of Udine Model Solution validator')
    parser.add_argument('--inst', '-i', required = True, type = str, help='instance file')
    parser.add_argument('--sol', '-s', required = True, type = str, help='solution file')
    parser.add_argument('--inst-format', '-if', required = False, default=Instance.default_format, choices=Instance.formats, type = str, help='instance format name')
    parser.add_argument('--sol-format', '-sf', required = False, default=Solution.default_format, choices=Solution.formats, type = str, help='solution format name')
    args = parser.parse_args()

    # open instance file
    f = open(args.inst)
    inst_content = f.read()
    f.close()

    # open solution file
    f = open(args.sol)
    sol_content = f.read()
    f.close()

    # activate validator, print result
    result = SolutionValidator.validate(inst_content, args.inst_format, sol_content, args.sol_format)
    print(json.dumps(result, indent=4))


# when invoked from the command line, call main()
if __name__ == '__main__':
    main()
