#!/usr/bin/env python3
import argparse
import json
import sys
from itertools import combinations
from instance_loader import *

# if validator is used standalone, import symbols from instance_loader module
if __name__ == '__main__':
    from instance_loader import *

class InstanceValidator(object): 
    '''Instance validation logic'''    
    # 
    @classmethod
    def my_assert_eq(cls, v1, v2, message):
        if v1 != v2:
            raise ValueError(message.format(v1, v2))
            
    @classmethod
    def my_assert_leq(cls, v1, v2, message):
        if v1 > v2:
            raise ValueError(message.format(v1, v2))
    
    @classmethod
    def validate(cls, inst_content, inst_format):
        '''Takes an instance as input string, evaluates its validity, computes features'''

        # correctly handle None format
        if not inst_format:
            inst_format = Instance.default_format
            
        # initialize feature map
        features = {}

        i = Instance(inst_content, inst_format) 
        features['courses'] = len(i.courses)
        features['periods'] = i.periods
        features['slots_per_day'] = i.slots_per_day
        features['rooms'] = len(list(filter(lambda r: r.type != 'Composite', i.rooms.values())))
        features['composite_rooms'] = len(list(filter(lambda r: r.type == 'Composite', i.rooms.values())))
        features['events'] = sum(c.number_of_exams * len(c.parts) for c in i.courses.values())
        # semantically check courses and compute a period density per course (the harmonic mean of the periods available for each exam)
        sum_of_available= 0
        n = 0
        empty_domains = []
        for course in i.courses.values():
            for exam in range(course.number_of_exams):
                for part in range(len(course.parts)):
                    # TODO: what if there is no available period
                    sum_of_available = len(course.available_periods[exam][part])
                    n += 1
                    if course.available_periods[exam][part] == set():
                        empty_domains.append((course.name, exam, course.parts[part]))        
        features['available_periods_density'] = sum_of_available / (i.periods * n)
        
        overall_sum_of_curricula = 0
        features['curriculum_periods_density'] = { 'primary': 0, 'secondary': 0, 'overall': 0 }
        for curriculum in i.curricula.values():
            for t in ('primary', 'secondary'):
                features['curriculum_periods_density'][t] += sum(map(lambda c: c.number_of_exams * len(c.parts), curriculum[t])) / i.periods
        features['curriculum_periods_density']['overall'] = sum(features['curriculum_periods_density'][t] for t in ('primary', 'secondary'))
        features['curriculum_periods_density'] = { t: features['curriculum_periods_density'][t] / len(i.curricula) for t in features['curriculum_periods_density'].keys()}
                
        r = {
            'valid': not empty_domains,
            'format': inst_format,
            'features': features
        }        
        if empty_domains:
            r['reason'] = { 
                "message": ["Course {}, exam {}, part {} has an empty set of available periods".format(*e) for e in empty_domains]
            }
        return r

def main():
    '''Entry point for when the validator is called from the command line, returns valid JSON'''

    # setup CLI
    parser = argparse.ArgumentParser(description='Instance validator')
    parser.add_argument('--inst-file', '-i', required = True, type = str, help='instance file')
    parser.add_argument('--inst-format', '-f', required = False, type = str, choices = Instance.formats, default=Instance.default_format, help='instance format name')
    args = parser.parse_args()

    with open(args.inst_file) as f:
        inst_content = f.read()
    # activate validator, print result
    result = InstanceValidator.validate(inst_content, args.inst_format)
    print(json.dumps(result, indent=4))


# when invoked from the command line, call main()
if __name__ == '__main__':
    main()
