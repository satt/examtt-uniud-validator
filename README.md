# Solution Validator for the University of Udine Examination Timetabling format

The purpose of this package is to provide a solution validator for the results of the University of Udine Examination Timetabling problem. The package is installable by means of the `pip` package installer, issuing the following command:


```bash
pip3 install git+https://bitbucket.org/satt/examtt-uniud-validator
```

Once installed, the validator is available in form of a script named `examtt-uniud-validator`, which is usually installed in the path, according to your python settings.

In order to use it you can issue the following command:

```bash
examtt-uniud-validator --inst <instance_file.json> --sol <solution_file.json>
```

where `<instance_file.json>` is the path of the instance file which the solution refers to, whereas `<solution_file.json>` contains the solution. Both files are expected to be in the reference `json` format.
